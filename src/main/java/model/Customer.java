package model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by viilveer on 06.10.2016.
 */
@Data
@Entity
@NoArgsConstructor
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Customer {
    @Id
    @SequenceGenerator(name = "customer_seq", sequenceName = "seq_customer", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_seq")
    private Long id;
    @NotNull
    @Size(min = 2, max = 15)
    @Pattern(regexp = "^[A-Za-z0-9]+$")
    private String firstName;
    @NotNull
    @Size(min = 2, max = 15)
    @Pattern(regexp = "^[A-Za-z0-9]+$")
    private String lastName;
    private String type;
    @NotNull
    @Size(min = 2, max = 15)
    @Pattern(regexp = "^[A-Za-z0-9]+$")
    private String code;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", nullable = false)
    private Collection<Phone> phones = new ArrayList<>();


    public Customer(Long id, String firstName, String lastName, String type, String code) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.type = type;
        this.code = code;
    }
}
