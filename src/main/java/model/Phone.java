package model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
public class Phone {
    @Id
    @SequenceGenerator(name = "phone_seq", sequenceName = "seq_phone", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "phone_seq")
    private Long id;

    private String value;

    private String type;

    public Phone(String value, String type) {
        this.value = value;
        this.type = type;
    }

}
