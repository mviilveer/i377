package model;

import java.util.Arrays;
import java.util.List;

/**
 * Created by viilveer on 17.10.2016.
 */
public class Classifiers {
    public List<String> customerTypes = Arrays.asList(
            "customer_type.private",
            "customer_type.corporate"
    );
    public List<String> phoneTypes = Arrays.asList(
            "phone_type.fixed",
            "phone_type.mobile"
    );
}
