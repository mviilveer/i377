package dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by viilveer on 11.10.2016.
 */
public class BaseDao {

    @PersistenceContext
    private EntityManager em;

    protected EntityManager getEntityManager() {
       return em;
    }
}
