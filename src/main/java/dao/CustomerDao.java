package dao;

import model.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by viilveer on 11.10.2016.
 * Performs customer data accessing from memory
 */
@Repository
@Transactional
public class CustomerDao extends BaseDao {
    public Customer getSingleById(Long id) {
        return getEntityManager().find(Customer.class, id);
    }
    public List<Customer> getAll() {
        TypedQuery<Customer> query = getEntityManager().createQuery("select distinct c from Customer as c left join fetch c.phones ", Customer.class);
        return query.getResultList();
    }

    public void save(Customer customer) {
        if (customer.getId() == null) {
            getEntityManager().persist(customer);
        } else {
            getEntityManager().merge(customer);
        }
    }

    public void delete(Long id) {
        Customer customer = getEntityManager().find(Customer.class, id);
        getEntityManager().remove(customer);
    }

    public void deleteAll() {
        getEntityManager().createQuery("delete from Phone").executeUpdate();
        getEntityManager().createQuery("delete from Customer").executeUpdate();
    }

    public List<Customer> findByKey(String key) {
        TypedQuery<Customer> query = getEntityManager().createQuery(
            "select distinct c from Customer as c left join fetch c.phones " +
                    "where " +
                    "lower(c.firstName) LIKE :key OR " +
                    "lower(c.lastName) LIKE :key OR " +
                    "lower(c.code) LIKE :key",
            Customer.class
        );
        return query.setParameter("key", '%' + key + '%').getResultList();
    }
}
