package validation;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
public class ValidationAdvice {

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler
    public ValidationErrors handleMethodArgumentNotValid(MethodArgumentNotValidException exception) {
        ValidationErrors validationErrors = new ValidationErrors();
        for (FieldError fieldError : exception.getBindingResult().getFieldErrors()) {
            validationErrors.addError(fieldError);
        }

        return validationErrors;
    }
}