package controller;

import dao.CustomerDao;
import model.Customer;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CustomerController {

    @Resource
    private CustomerDao dao;

    @GetMapping("customers")
    public List<Customer> getCustomers()
    {
        return dao.getAll();
    }

    @GetMapping("customers/search")
    public List<Customer> findCustomers(@RequestParam("key") String key)
    {
        return dao.findByKey(key);
    }

    @PostMapping("customers")
    public void insertCustomer(@RequestBody @Valid Customer customer)
    {
        dao.save(customer);
    }

    @DeleteMapping("customers/{id}")
    public void insertCustomer(@PathVariable Long id)
    {
        dao.delete(id);
    }

    @DeleteMapping("customers")
    public void deleteAllCustomers()
    {
        dao.deleteAll();
    }

    @GetMapping("customers/{id}")
    public Customer getCustomer(@PathVariable Long id)
    {
        return dao.getSingleById(id);
    }

}