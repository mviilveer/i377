package controller;

import model.Classifiers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClassifierController {

    @GetMapping("classifiers")
    public Classifiers getClassifiers()
    {
        return new Classifiers();
    }
}
