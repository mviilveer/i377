(function () {
    'use strict';

    var app = angular.module('app', ['ngRoute']);

    app.constant('RESOURCES', (function () {
        var resource = 'http://localhost:8080';
        return {
            CUSTOMERS_DOMAIN: resource,
            CUSTOMERS: resource + '/api/customers/',
            CUSTOMER_SEARCH: resource + '/api/customers/search',
            CLASSIFIERS: resource + '/api/classifiers'
        }
    })());
})();