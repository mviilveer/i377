(function () {
    'use strict';

    angular.module('app').config(routeConfig);

    function routeConfig($routeProvider) {
        $routeProvider.when('/search', {
            templateUrl : 'app/templates/list.html',
            controller: 'SearchCtrl',
            controllerAs: 'vm',
            resolve: {
                customers: function ($http, RESOURCES) {
                    return $http.get(RESOURCES.CUSTOMERS);
                }
            }
        }).when('/new', {
            templateUrl : 'app/templates/edit.html',
            controller: 'EditCtrl',
            controllerAs: 'vm',
            resolve: {
                existingCustomer: function () {
                    return null;
                },
                classifiers: function ($http, RESOURCES) {
                    return $http.get(RESOURCES.CLASSIFIERS);
                }
            }
        }).when('/view/:id', {
            templateUrl : 'app/templates/edit.html',
            controller: 'EditCtrl',
            controllerAs: 'vm',
            resolve: {
                existingCustomer: function ($http, $route, RESOURCES) {
                    return $http.get(RESOURCES.CUSTOMERS + $route.current.params.id)
                },
                classifiers: function ($http, RESOURCES) {
                    return $http.get(RESOURCES.CLASSIFIERS);
                }
            }
        }).otherwise('/search');
    }

})();