(function () {
    'use strict';

    angular.module('app').controller('EditCtrl', Ctrl);

    function Ctrl($http, $location, existingCustomer, classifiers, RESOURCES) {
        var vm = this;
        vm.save = save;
        vm.existingCustomer = existingCustomer;
        vm.removeChoice = removeChoice;
        vm.addNewChoice = addNewChoice;
        vm.customerTypes = {};
        vm.phoneTypes = {};

        vm.customerTypes = classifiers.data.customerTypes;
        vm.phoneTypes = classifiers.data.phoneTypes;

        vm.errors = [];
        vm.form = {
            firstName: null,
            lastName: null,
            code: null,
            type: null,
            phones: []
        };

        if (existingCustomer) {
            vm.form = existingCustomer.data;
        }

        function save() {
            $http.post(RESOURCES.CUSTOMERS, vm.form).then(function (result) {
                $location.path('/list');
            }).catch(function (result) {
                vm.errors = result.data.errors;
            });
        }

        function addNewChoice() {
            vm.form.phones.push({});
        }

        function removeChoice(index) {
            vm.form.phones.splice(index, 1);
        }
    }
})();
