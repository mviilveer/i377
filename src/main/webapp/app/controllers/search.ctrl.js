(function () {
    'use strict';

    angular.module('app').controller('SearchCtrl', Ctrl);

    function Ctrl($http, RESOURCES, customers) {
        var vm = this;
        vm.customers = customers.data;
        vm.searchString = null;
        vm.remove = remove;
        vm.search = search;

        function search() {
            var searchServer = RESOURCES.CUSTOMERS;
            if (vm.searchString) {
                searchServer = RESOURCES.CUSTOMER_SEARCH;
            }
            $http.get(searchServer, {params: { key: vm.searchString }}).then(function (result) {
                vm.customers = result.data;
            });
        }

        function remove(id) {
            $http.delete(RESOURCES.CUSTOMERS + id).then(function (result) {
                search();
            });
        }
    }
})();
